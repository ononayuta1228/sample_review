// Imports the Flutter Driver API.
import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';



void main() {
  group('AwaseAppの起動テスト', () {
    final searchFinder = find.byValueKey('search');
    final homeFinder = find.byValueKey('home');
    final pageFinder = find.byValueKey('page');
    final loginFinder = find.byValueKey('login');
    final loginButtonFinder = find.byValueKey('login_name');

    FlutterDriver driver;

    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    test('検索ページを開くテスト', () async {
      await driver.tap(searchFinder);
      expect(await driver.getText(pageFinder), "1");
    });

    test('ログイン画面を開くテスト', () async {
      await driver.tap(homeFinder);
      await driver.tap(loginFinder);
      expect(await driver.getText(loginButtonFinder), "ログイン");
    });

    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });


  });
}