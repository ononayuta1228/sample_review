import 'package:flutter/material.dart';
import 'package:awase_app/screens/event_list.dart';

import 'package:awase_app/navigator.dart';
import 'package:awase_app/repositories/current_user_repository.dart';
import 'package:awase_app/widgets/sign_in.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final CurrentUserRepository currentUser;

  MyApp()
      : this.currentUser = CurrentUserRepository(),
        super();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: <String, WidgetBuilder>{
        Nav.SIGN_IN: (BuildContext context) =>
            new SignInPage(currentUser: currentUser),
      },
      title: 'Flutter Demo',
      theme: ThemeData(
       primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: "App"),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
final String title;

  final CurrentUserRepository currentUser = CurrentUserRepository();

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  static final CurrentUserRepository _currentUser = CurrentUserRepository();
  int _selectedIndex = 0;

  List<Widget> _pages() => [
        EventList(currentUser: _currentUser),
        Text(
          '$_selectedIndex',
          key: Key("page"),
        ),
      ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _pages().elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home,
                key: Key('home'),
              ),
              title: Text('ホーム'),
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.search,
                    key: Key("search")),
                title: Text('検索')),
            BottomNavigationBarItem(
                icon: Icon(Icons.account_box),
                title: Text('アカウント')),
            BottomNavigationBarItem(
                icon: Icon(Icons.settings),
                title: Text('設定')),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: Colors.amber[800],
          unselectedItemColor: Colors.grey[800],
          onTap: _onItemTapped),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).pushNamed(Nav.SIGN_IN);
        },
        tooltip: 'Increment',
        child: Icon(Icons.add),
        key: Key('login'),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
