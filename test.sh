set -e
echo 'Test start!'
flutter test test/models/user_test.dart
flutter test test/main_test.dart
flutter drive --target=test_driver/app.dart
echo 'Test Succeed!!'